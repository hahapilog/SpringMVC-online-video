<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/bootstrap.min.css">
    <!-- 自己的 CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/video.css">
    <!-- awesom CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/font-awesome.min.css">


    <title>工具</title>

</head>

<%--onload="$('#userInfo').hide()"--%>
<body>

<!-- ====导航栏==== -->

<jsp:include page="common/header.jsp"/>


<br>
<br>

<div class="container">
    <!-- 视频播放 -->
    <div class="card">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe src="//player.bilibili.com/player.html?aid=970441268&bvid=BV1ep4y1r7Eo&cid=260830422&page=1"
                    allowfullscreen="allowfullscreen"
                    scrolling="no" frameborder="0">

            </iframe>
        </div>
    </div>
    <!-- 信息展示 -->
    <div class="row mt-2">
        <div class="col-md-8">
            <span> 浏览次数：${course.views}</span>
            <span> 课时数：${videoList.size()}</span>
            <span> 类别：${course.vipFlag == 0 ? "免费" : "会员"}</span>
            <span><i class="fa fa-share-alt text-primary" aria-hidden="true"></i>&nbsp;分享</span>
            <span><i class="fa fa-star text-primary" aria-hidden="true"></i>&nbsp;收藏</span>
        </div>
        <div class="col-md-4 float-right">
            <a class="btn" href="${course.coursewareUrl}"><p class="btn btn-info">资料下载</p></a>
            <p class="btn btn-info">在线咨询</p>
        </div>

    </div>

    <div class="row mt-2">
        <div class="col-md-8">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                       aria-controls="home"
                       aria-selected="true">目录</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                       aria-controls="profile"
                       aria-selected="false">介绍</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <ul class="list-group list-group-flush">

                        <c:forEach items="${videoList}" var="video">
                            <li class="list-group-item list-group-item-action" onclick="videoChange('${video.videoUrl1}',this)">
                                    ${video.videoName}
                                <c:if test="${video.treeView == 0}">
                                        <span type='button' class='badge badge-pill badge-success badge-success'>免费</span>
                                </c:if>
                                <c:if test="${video.treeView == 1}">
                                        <span type='button' class='badge badge-pill badge-danger'>会员</span>
                                </c:if>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">${course.intro}</div>
            </div>
        </div>
        <div class="col-md-4 float-right">
            <div class="card border-0">
                <span class="card-header">在线视频</span>
                <div class="card-body">
                    <span>  <i class="fa fa-star fa-5x"></i>管理员：</span>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- ====页脚====  -->
<div>
    <jsp:include page="common/footer.jsp"/>
</div>

<%--<input type="button" value="自动登录" class="small-rounded" onclick="toggleInfoLogin()">--%>


<script type="application/javascript">
    window.onload = function () {
        $('#registBtn').on("click", changeImg);
        <c:choose>
        <c:when test="${sessionScope.login_user != null}">

        toggleInfoLogin();

        </c:when>
        </c:choose>

    }

    //  获取新的验证码图片
    function changeImg() {
        $('#registVcodeImg')[0].src = "captcha?rd=" + Math.random() * 100;
    }

    //  检查邮箱
    function CheckEmail(obj) {
        $.ajax({
            url: "checkEmail?email=" + obj.value,
            success: function (result) {
                var rCode = result.rCode;
                if (rCode == 1) {
                    //  可以注册
                    $('#registEmail').removeClass("is-invalid");
                    $('#registEmail').addClass("is-valid");
                } else {
                    //  不可以注册
                    $('#registEmail').removeClass("is-valid");
                    $('#registEmail').addClass("is-invalid");
                }
            }
        });
    }

    //  检查验证码
    function checkVcode() {
        var vcode = $('#registVcode').val()
        var flag = false;
        $.ajax({
            url: "checkRegistCaptcha?captcha=" + vcode,
            success: function (result) {
                if (result.rCode == 1) {
                    $('#registVcode').removeClass("is_invalid");
                    flag = true;
                    console.log("验证通过");
                } else {
                    $('#registVcode').addClass("is-invalid");
                    console.log("验证不通过");
                }
            },
            async: false
        })
        return flag;
    }

    //  取消警告
    function cancelWarn(obj) {
        $(obj).removeClass("is-invalid");
    }

    // 登录
    function login() {
        $.ajax({
            url: "login",
            type: "POST",
            data: {
                password: $('#loginPass').val(),
                email: $('#loginEmail').val(),
                autoLogin: $('#autoLogin').is(':checked')
            },
            success: function (result) {
                if (result.rCode == 1) {
                    //  登录成功
                    var user = result.data;
                    toggleInfoLogin();
                    $('#loginClose').click();
                    $("#userShow").text(user.email);

                } else {
                    //  登录失败
                    $("#loginEmail").addClass("is-invalid");
                    $("#loginEmail").on("input", removeLoginTips);
                    $("#loginPass").on("input", removeLoginTips);
                }
            }
        })
        return false;
    }

    //  移除登录失败提示事件
    function removeLoginTips() {
        $("#loginEmail").removeClass("is-invalid");
    }

    //  切换登录和用户信息展示
    function toggleInfoLogin() {
        $('#loginOrRegist').toggle();
        $("#userInfo").toggle();
    }

</script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../static/js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../static/js/bootstrap.min.js"></script>
<script src="../../static/js/video.js"></script>
</body>

</html>
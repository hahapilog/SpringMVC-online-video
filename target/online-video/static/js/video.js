function videoChange(url,obj) {
    $("iframe[src]").attr("src",url);
    reActive(obj);
}

function reActive(obj) {
    var lis = $(obj).parent().children();
        $(lis).removeClass("active");
    $(obj).addClass("active");

}

//  获取新的验证码图片
function changeImg() {
    $('#registVcodeImg')[0].src = "captcha?rd=" + Math.random() * 100;
}

//  检查邮箱
function CheckEmail(obj) {
    $.ajax({
        url: "checkEmail?email=" + obj.value,
        success: function (result) {
            var rCode = result.rCode;
            if (rCode == 1) {
                //  可以注册
                $('#registEmail').removeClass("is-invalid");
                $('#registEmail').addClass("is-valid");
            } else {
                //  不可以注册
                $('#registEmail').removeClass("is-valid");
                $('#registEmail').addClass("is-invalid");
            }
        }
    });
}

//  检查验证码
function checkVcode() {
    var vcode = $('#registVcode').val()
    var flag = false;
    $.ajax({
        url: "checkRegistCaptcha?captcha=" + vcode,
        success: function (result) {
            if (result.rCode == 1) {
                $('#registVcode').removeClass("is_invalid");
                flag = true;
                console.log("验证通过");
            } else {
                $('#registVcode').addClass("is-invalid");
                console.log("验证不通过");
            }
        },
        async: false
    })
    return flag;
}

//  取消警告
function cancelWarn(obj) {
    $(obj).removeClass("is-invalid");
}

// 登录
function login() {
    $.ajax({
        url: "login",
        type: "POST",
        data: {
            password: $('#loginPass').val(),
            email: $('#loginEmail').val(),
            autoLogin: $('#autoLogin').is(':checked')
        },
        success: function (result) {
            if (result.rCode == 1) {
                //  登录成功
                var user = result.data;
                toggleInfoLogin();
                $('#loginClose').click();
                $("#userShow").text(user.email);

            } else {
                //  登录失败
                $("#loginEmail").addClass("is-invalid");
                $("#loginEmail").on("input", removeLoginTips);
                $("#loginPass").on("input", removeLoginTips);
            }
        }
    })
    return false;
}

//  移除登录失败提示事件
function removeLoginTips() {
    $("#loginEmail").removeClass("is-invalid");
}

//  切换登录和用户信息展示
function toggleInfoLogin() {
    $('#loginOrRegist').toggle();
    $("#userInfo").toggle();
}

//  搜索视频事件
function search() {
    alert(55555555);
    var keyword = $("#keyword").val();
    var url = "/search/" + keyword + "/1";
    $("#search").attr("href",url);
    $("#search")[0].click();
}

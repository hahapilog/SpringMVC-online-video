<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/bootstrap.min.css">
    <!-- 自己的 CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/video.css">
    <title>工具</title>

</head>

<%--onload="$('#userInfo').hide()"--%>
<body>

<!-- ====导航栏==== -->

<jsp:include page="common/header.jsp"/>


<br>
<br>
<!-- 工具分类导航 -->
<div class="container">
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link ${typeId == 0 ? "active" : ""}" href="/tools/0">全部</a>
        </li>
        <c:forEach items="${toolsTypeList}" var="toolType">
            <li class="nav-item">
                <a class="nav-link ${typeId == toolType.id ? "active" : ""}" href="/tools/${toolType.id}">${toolType.name}</a>
            </li>
        </c:forEach>

    </ul>
</div>
<!-- 工具展示 -->
<br>
<br>
<div class="container text-monospace clearfix navbar-fixed-bottom">
    <div class="row">
        <c:forEach items="${toolList.list}" var="tool">
            <div class="col-md-3">
                <div class="my_card select-shadown">
                    <a href="${tool.toolUrl}">
                        <img src="../../static/imgs/356.jpg" class="card-img-top" alt="...">
                    </a>
                    <div class="card-body clearfix text-center">
                        <a href="${tool.toolUrl}" class="text-decoration-none">
                            ${tool.name}
                        </a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<!--    分页      -->
<div class="container">
    <div class="row  justify-content-md-center">
        <div class="col-md-auto mt-5">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item ${toolList.isFirstPage ? "disabled" : ""}"><a class="page-link" href="/tools/${typeId}?page=${toolList.prePage}">上一页</a></li>
                    <c:forEach items="${toolList.navigatepageNums}" var="pageIndex">
                        <li class="page-item"><a class="page-link" href="/tools/${typeId}?page=${pageIndex}">${pageIndex}</a></li>
                    </c:forEach>
                    <li class="page-item ${toolList.isLastPage ? "disabled" : ""}"><a class="page-link" href="/tools/${typeId}?page=${toolList.nextPage}">下一页</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<!-- ====页脚====  -->
<div>
    <jsp:include page="common/footer.jsp"/>
</div>

<%--<input type="button" value="自动登录" class="small-rounded" onclick="toggleInfoLogin()">--%>


<script type="application/javascript">
    window.onload = function () {
        $('#registBtn').on("click", changeImg);
        <c:choose>
        <c:when test="${sessionScope.login_user != null}">

        toggleInfoLogin();

        </c:when>
        </c:choose>

    }

    //  获取新的验证码图片
    function changeImg() {
        $('#registVcodeImg')[0].src = "captcha?rd=" + Math.random() * 100;
    }

    //  检查邮箱
    function CheckEmail(obj) {
        $.ajax({
            url: "checkEmail?email=" + obj.value,
            success: function (result) {
                var rCode = result.rCode;
                if (rCode == 1) {
                    //  可以注册
                    $('#registEmail').removeClass("is-invalid");
                    $('#registEmail').addClass("is-valid");
                } else {
                    //  不可以注册
                    $('#registEmail').removeClass("is-valid");
                    $('#registEmail').addClass("is-invalid");
                }
            }
        });
    }

    //  检查验证码
    function checkVcode() {
        var vcode = $('#registVcode').val()
        var flag = false;
        $.ajax({
            url: "checkRegistCaptcha?captcha=" + vcode,
            success: function (result) {
                if (result.rCode == 1) {
                    $('#registVcode').removeClass("is_invalid");
                    flag = true;
                    console.log("验证通过");
                } else {
                    $('#registVcode').addClass("is-invalid");
                    console.log("验证不通过");
                }
            },
            async: false
        })
        return flag;
    }

    //  取消警告
    function cancelWarn(obj) {
        $(obj).removeClass("is-invalid");
    }

    // 登录
    function login() {
        $.ajax({
            url: "login",
            type: "POST",
            data: {
                password: $('#loginPass').val(),
                email: $('#loginEmail').val(),
                autoLogin: $('#autoLogin').is(':checked')
            },
            success: function (result) {
                if (result.rCode == 1) {
                    //  登录成功
                    var user = result.data;
                    toggleInfoLogin();
                    $('#loginClose').click();
                    $("#userShow").text(user.email);

                } else {
                    //  登录失败
                    $("#loginEmail").addClass("is-invalid");
                    $("#loginEmail").on("input", removeLoginTips);
                    $("#loginPass").on("input", removeLoginTips);
                }
            }
        })
        return false;
    }

    //  移除登录失败提示事件
    function removeLoginTips() {
        $("#loginEmail").removeClass("is-invalid");
    }

    //  切换登录和用户信息展示
    function toggleInfoLogin() {
        $('#loginOrRegist').toggle();
        $("#userInfo").toggle();
    }

</script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../static/js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../static/js/bootstrap.min.js"></script>
</body>

</html>
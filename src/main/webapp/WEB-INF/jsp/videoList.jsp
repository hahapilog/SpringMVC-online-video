<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/bootstrap.min.css">
    <!-- 自己的 CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/video.css">
    <title>课程</title>
    <script type="application/javascript">
        window.onload = function () {
            $('#registBtn').on("click", changeImg);
            <c:choose>
            <c:when test="${sessionScope.login_user != null}">

            toggleInfoLogin();

            </c:when>
            </c:choose>

        }
    </script>

</head>

<%--onload="$('#userInfo').hide()"--%>
<body>

<!-- ====导航栏==== -->

<jsp:include page="common/header.jsp"/>

<br><br>


<br>
<div class="container">
    <!-- 视频播放 -->
    <div class="card">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe src="#"
                    allowfullscreen="allowfullscreen"
                    scrolling="no" frameborder="0">
            </iframe>
        </div>
    </div>
    <hr>
    <!--    视频展示      -->
    <div class="container text-monospace clearfix">
        <div class="row">
            <c:forEach items="${videoPageInfo.list}" var="video">
                <div class="col-md-3 mt-3">
                    <div onclick="videoChange('${video.videoUrl1}',this)">
                        <div class="card select-shadown">

                            <img src="/static/imgs/banner1.jpg" class="card-img-top" alt="...">
                            <p class="card-text ">${video.videoName}</p>
                            <div class="card-body clearfix">
                                <c:choose>
                                    <c:when test="${video.treeView == 1}">
                                        <span type="button" class="badge float-right">会员</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span type="button" class="badge float-right">免费</span>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <!--    分页      -->
    <div class="container">
        <div class="row  justify-content-md-center">
            <div class="col-md-auto mt-5">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item ${videoPageInfo.isFirstPage ? "disabled" : ""}"><a class="page-link"
                                                                                          href="/courseList/${keyword}/${videoPageInfo.prePage}">上一页</a>
                        </li>
                        <c:forEach items="${videoPageInfo.navigatepageNums}" var="pageIndex">
                            <li class="page-item"><a class="page-link"
                                                     href="/courseList/${keyword}/${pageIndex}">${pageIndex}</a>
                            </li>
                        </c:forEach>
                        <li class="page-item ${videoPageInfo.isLastPage ? "disabled" : ""}"><a class="page-link"
                                                                                         href="/courseList/${keyword}/${videoPageInfo.nextPage}">下一页</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

</div>

<!-- ====页脚====  -->
<div style="position: absolute;left: 30%;bottom: 0%">
    <jsp:include page="common/footer.jsp"/>
</div>

<%--<input type="button" value="自动登录" class="small-rounded" onclick="toggleInfoLogin()">--%>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../static/js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../static/js/bootstrap.min.js"></script>
<script src="../../static/js/video.js"></script>
</body>

</html>
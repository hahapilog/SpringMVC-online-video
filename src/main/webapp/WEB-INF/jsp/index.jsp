<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/bootstrap.min.css">
    <!-- 自己的 CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/video.css">
    <title>首页</title>

</head>

<%--onload="$('#userInfo').hide()"--%>
<body>

<!-- ====导航栏==== -->

<jsp:include page="common/header.jsp"/>

<!-- ====轮播====  -->

<br><br>
<div id="carouselExampleIndicators" class="carousel slide container " data-ride="carousel">
    <ol class="carousel-indicators">
        <c:forEach items="${bannerList}" var="banner" varStatus="idx" begin="0">
            <li data-target="#carouselExampleIndicators" data-slide-to="${idx.index}" <c:if test="${idx.index == 0}"> class="active"</c:if>></li>
        </c:forEach>
    </ol>
    <div class="carousel-inner">
        <c:forEach items="${bannerList}" var="banner" varStatus="idx" begin="0">
            <div class="carousel-item <c:if test="${idx.index == 0}">active</c:if>">
                <a href="${banner.targetUrl}">
                    <img src="../../static/imgs/banner1.jpg" class="small-rounded d-block w-100" alt="...">
                </a>
            </div>
        </c:forEach>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- ====课程展示====  -->

<!-- 最新课程 -->
<br>
<div class="container">
    <div class="clearfix">
        <h4 class="text-center text-size">最新课程</h4>
        <a href="/courseList/0" class="float-right">更多 ></a>
    </div>
    <hr>
    <div class="container text-monospace clearfix">
        <div class="row">
            <c:forEach items="${newestCourse.list}" var="course">
                <div class="col-md-3">
                    <div class="my_card select-shadown">
                        <a href="/courseVideo/${course.id}">
                            <img src="${course.showIco}" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body clearfix">
                            <a href="/courseVideo/${course.id}" class="text-decoration-none">

                                <p class="card-text ">${course.courseName}</p>
                                <p class="card-text float-left">学习人数：${course.views}人</p>
                            </a>
                            <c:choose>
                                <c:when test="${course.vipFlag == 0}">
                                    <span type="button" class="badge float-right">免费</span>
                                </c:when>
                                <c:otherwise>
                                    <span type="button" class="badge text-primary float-right">会员</span>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<!-- Spring系列 -->
<br>
<div class="container">
    <div class="clearfix">
        <h4 class="text-center text-size">Spring系列</h4>
        <a href="/courseList/${type4.list.get(0).typeId}" class="float-right">更多 ></a>
    </div>
    <hr>
    <div class="container text-monospace clearfix">
        <div class="row">
            <c:forEach items="${type4.list}" var="course">
                <div class="col-md-3">
                    <div class="my_card select-shadown">
                        <a href="/courseVideo/${course.id}">
                            <img src="${course.showIco}" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body clearfix">
                            <a href="/courseVideo/${course.id}" class="text-decoration-none">

                                <p class="card-text ">${course.courseName}</p>
                                <p class="card-text float-left">学习人数：${course.views}</p>
                            </a>

                            <span type="button" class="float-right badge">${course.vipFlag == 0 ? "免费" : "会员"}</span>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<!-- ====页脚====  -->
<jsp:include page="common/footer.jsp"/>


<%--<input type="button" value="自动登录" class="small-rounded" onclick="toggleInfoLogin()">--%>


<script type="application/javascript">
    window.onload = function onload() {
        $('#registBtn').on("click", changeImg);
        <c:choose>
        <c:when test="${sessionScope.login_user != null}">

        toggleInfoLogin();

        </c:when>
        </c:choose>

    }
</script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../static/js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../static/js/bootstrap.min.js"></script>
<script src="../../static/js/video.js"></script>
</body>

</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 导航栏 -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/">在线视频</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <c:if test="${navFocus ==1}">active</c:if>">
                    <a class="nav-link" href="/">首页<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <c:if test="${navFocus == 2}">active</c:if>">
                    <a class="nav-link" href="/courseList/0">课程</a>
                </li>
                <li class="nav-item <c:if test="${navFocus == 3}">active</c:if>">
                    <a class="nav-link" href="/vip">会员</a>
                </li>
                <li class="nav-item <c:if test="${navFocus == 4}">active</c:if>">
                    <a class="nav-link" href="https://www.bilibili.com/">直播</a>
                </li>
                <li class="nav-item <c:if test="${navFocus == 5}">active</c:if>">
                    <a class="nav-link" href="/tools/0">工具</a>
                </li>
            </ul>
            <%--      用户信息展示 默认隐藏      --%>
            <div class="dropdown" id="userInfo" style="display: none">
                <button class="btn dropdown-toggle text-primary" type="button" id="userShow" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    ${sessionScope.login_user.email}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="exit">退出</a>
                </div>
            </div>
            <%--      登录和注册入口展示      --%>
            <div id="loginOrRegist">
                <a href="#" data-toggle="modal" data-target="#loginModal" data-whatever="@mdo"
                   class="mr-2 text-primary">登录</a>/
                <a href="#" data-toggle="modal" data-target="#registModal" data-whatever="@fat"
                   class="ml-2 mr-4 text-primary" id="registBtn">注册</a>
            </div>
            <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" id="keyword" name="keyword" placeholder="搜索视频" aria-label="Search">
                <a id="search" href="#">搜索</a>
                <button class="btn btn-outline-primary my-2 my-sm-0" type="button" onclick="search()">搜索</button>

            </div>
        </div>
    </div>
</nav>

<!-- 登录 -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form onsubmit="return login()">
                    <div class="form-row">
                        <div class="col-md-12 mb-3 container">
                            <label for="loginEmail">邮箱</label>
                            <input type="email" class="form-control" id="loginEmail" value="" required>
                            <div class="invalid-feedback">
                                用户名或密码错误
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3 container">
                            <label for="loginPass">密码</label>
                            <input type="text" class="form-control" id="loginPass" value="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="autoLogin">
                            <label class="form-check-label" for="autoLogin">
                                自动登录
                            </label>
                            <a href="RetrievePassword.html" class="float-right">忘记密码？</a>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#registModal"
                           class="mr-auto bd-highlight">还没有账号，点我注册！</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="loginClose">关闭</button>
                        <button type="submit" class="btn btn-primary">登录</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- 注册 -->
<div class="modal fade" id="registModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form action="regist" method="get" onsubmit="return checkVcode()">
                    <div class="form-row">
                        <div class="col-md-12 mb-3 container">
                            <label for="registEmail">邮箱</label>
                            <input type="email" name="email" value="" class="form-control" id="registEmail"
                                   onblur="CheckEmail(this)" required>
                            <div class="valid-feedback">
                                可以注册
                            </div>
                            <div class="invalid-feedback">
                                此邮箱已注册
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3 container">
                            <label for="registMobile">手机</label>
                            <input type="text" pattern="^1[3-9]\d{9}$" placeholder="请输入11位手机号码" name="mobile"
                                   value="" class="form-control" id="registMobile" required>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3 container">
                            <label for="registPassword">密码</label>
                            <input type="text" pattern="^(?=.*[A-Za-z].*)[a-zA-Z0-9_]{8,16}$"
                                   placeholder="请输入密码(8至16位且至少包含一个字母)" name="password" value="" class="form-control"
                                   id="registPassword" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-7 mb-3">
                            <label for="registVcode">验证码</label>
                            <input id="registVcode" type="text" oninput="cancelWarn(this)" placeholder="请输入验证码"
                                   name="vcode" value="" class="form-control"
                                   required>
                            <div class="invalid-feedback">
                                验证码错误
                            </div>
                        </div>
                        <div class="col-md-5 mb-3 float-right">
                            <img src="#" onclick="changeImg()" id="registVcodeImg" class="container">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#loginModal"
                           class="mr-auto bd-highlight">已有账号，点我登录！</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                        <button type="submit" class="btn btn-primary" onsubmit="return checkVcode()">注册</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
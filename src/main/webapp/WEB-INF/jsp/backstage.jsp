<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/bootstrap.min.css">
    <!-- awesome CSS -->
    <link type="text/css" rel="stylesheet" href="../../static/css/font-awesome.min.css">
    <!-- 自己的 CSS -->
    <link type="text/css" rel="stylesheet" href="../static/css/video.css">
    <title>后台管理</title>

</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Company name</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse"
            data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="#">Sign out</a>
        </li>
    </ul>
</nav>
<div class="row">
    <jsp:include page="common/backstageList.jsp"/>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        <h2>Section title</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>电话</th>
                    <th>用户名</th>
                    <th>密码</th>
                    <th>邮箱</th>
                    <th>VIP</th>
                    <th>注册时间</th>
                    <th>编辑</th>
                    <th>删除</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${userList.list}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.mobile}</td>
                        <td>${user.username}</td>
                        <td>${user.password}</td>
                        <td>${user.email}</td>
                        <td>${user.vipFlag}</td>
                        <td>${user.createTime}</td>
                        <td>
                            <a href="#" onclick="$.confirm({message :'123'})">
                                <i class="fa fa-2x text-primary fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a href="/user/delete/${user.id}/${currentPage}">
                                </i><i class="fa fa-2x text-danger fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <!--    分页      -->
        <div class="container">
            <div class="row  justify-content-md-center">
                <div class="col-md-auto mt-5">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item ${userList.isFirstPage ? "disabled" : ""}"><a class="page-link"
                                                                                               href="/user/${userList.prePage}">上一页</a>
                            </li>
                            <c:forEach items="${userList.navigatepageNums}" var="pageIndex">
                                <li class="page-item"><a class="page-link" href="/user/${pageIndex}">${pageIndex}</a>
                                </li>
                            </c:forEach>
                            <li class="page-item ${userList.isLastPage ? "disabled" : ""}"><a class="page-link"
                                                                                              href="/user/${userList.nextPage}">下一页</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </main>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../../static/js/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="../../static/js/bootstrap.min.js"></script>
<script src="../../static/js/video.js"></script>

</body>
</html>
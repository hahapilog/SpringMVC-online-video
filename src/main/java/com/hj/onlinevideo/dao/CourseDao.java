package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CourseDao {
    void insertCourse(Course course);

    List<Course> findCourseByCondition(Map map);

    List<Course> findCourseByIds(List list);

}

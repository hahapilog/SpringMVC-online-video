package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.Banner;

import java.util.List;
import java.util.Map;

public interface BannerDao {
    void insertBanner(Banner banner);

    List<Banner> findBannerByCondition(Map map);
}

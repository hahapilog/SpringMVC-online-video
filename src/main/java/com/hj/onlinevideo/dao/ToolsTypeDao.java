package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.ToolsType;

import java.util.List;

public interface ToolsTypeDao {
    void insertToolsType(ToolsType toolsType);

    List<ToolsType> findToolsTypeAll();
}

package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.User;

import java.util.List;
import java.util.Map;

public interface UserDao {
    //  插入
    int insertUser(User user);

    //  修改
    int updateUser(Map map);

    //  查找
    User findUserByCondition(User user);

    //  更新
    int updateUser(User user);

    //  按页查找
    List<User> findUserByCondition(Map map);

    //  全部
    List<User> findUserAll();
}

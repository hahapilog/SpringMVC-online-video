package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.Video;

import java.util.List;
import java.util.Map;

public interface VideoDao {
    void insertVideo(Video video);
    List<Video> findVideoByCondition(Map map);
}

package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.Tool;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface ToolDao {

    List<Tool> findToolAll(Map map);

    List<Tool> findToolByCondition(Map map);

    void insertTool(Tool tool);
}

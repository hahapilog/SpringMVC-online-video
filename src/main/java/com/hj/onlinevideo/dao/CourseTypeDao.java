package com.hj.onlinevideo.dao;

import com.hj.onlinevideo.entity.CourseType;

import java.util.List;

public interface CourseTypeDao {
    void insertCourseType(CourseType courseType);
    List<CourseType> findCourseTypeAll();
}

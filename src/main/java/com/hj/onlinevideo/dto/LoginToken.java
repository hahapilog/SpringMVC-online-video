package com.hj.onlinevideo.dto;

import cn.hutool.crypto.digest.DigestUtil;
import com.hj.onlinevideo.entity.User;

/**
 *  存储登录token信息
 */
public class LoginToken implements Token{
    private String ip;
    private String now;
    private String userAgent;
    private User user;

    @Override
    public String toString() {
        return "LoginToken{" +
                "ip='" + ip + '\'' +
                ", now='" + now + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", user=" + user +
                '}';
    }

    public LoginToken(String ip, String now, String userAgent, User user) {
        this.ip = ip;
        this.now = now;
        this.userAgent = userAgent;
        this.user = user;
    }

    public LoginToken() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String generateMD5() {
        StringBuilder builder = new StringBuilder();
        builder.append(ip);
        builder.append(now);
        builder.append(userAgent);
        builder.append(user);
        String tokenMD5 = DigestUtil.md5Hex(builder.toString());
        return tokenMD5;
    }
}

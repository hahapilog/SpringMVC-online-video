package com.hj.onlinevideo.dto;

/**
 *  响应数据 JSON
 *
 * @param <T>
 */
public class ResponseData<T> {
    private Integer rCode;
    private T data;
    private String message;

    public ResponseData(Integer rCode, String message) {
        this.rCode = rCode;
        this.message = message;
    }

    public Integer getrCode() {
        return rCode;
    }

    public void setrCode(Integer rCode) {
        this.rCode = rCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseData(Integer rCode, T data, String message) {
        this.rCode = rCode;
        this.data = data;
        this.message = message;
    }

    public ResponseData() {
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "rCode='" + rCode + '\'' +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}

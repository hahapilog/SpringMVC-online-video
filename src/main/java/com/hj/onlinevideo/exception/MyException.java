package com.hj.onlinevideo.exception;

public class MyException extends RuntimeException {
    private String msg;
    public MyException () {}
    public MyException(String msg) {
        super(msg);
        this.msg = msg;
    }
}

package com.hj.onlinevideo.intercepter;

import cn.hutool.core.util.StrUtil;
import com.hj.onlinevideo.dto.LoginToken;
import com.hj.onlinevideo.entity.User;
import com.hj.onlinevideo.util.Constants;
import com.hj.onlinevideo.util.VideoUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Struct;
import java.util.Map;

@Component
public class AutoLoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        //  判断session中是否已经有User了
        System.out.println("拦截器执行");
        if (request.getSession().getAttribute(Constants.SESSION_LOGIN_USER) == null) {
            System.out.println("session判断不存在User");
            User user = VideoUtil.validationToken(request);
            if (user != null) {
                request.getSession().setAttribute(Constants.SESSION_LOGIN_USER, user);
            }
        }
        System.out.println("拦截器结束 ");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}

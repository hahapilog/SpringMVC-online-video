package com.hj.onlinevideo.service;

import com.hj.onlinevideo.entity.Banner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BannerService {
    List<Banner> findIndexBanner();
}

package com.hj.onlinevideo.service.impl;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *  邮件服务
 */
@Service
public class SendMailService {
    @Autowired
    JavaMailSender javaMailSender;
    public void sendMail(String link) throws MessagingException {
        //  先处理下正文 把链接加进来
        StringBuilder builder = new StringBuilder();
        builder.append("<h1>更改密码请点击链接：<a href='");
        builder.append(link);
        builder.append("'>"+link+"</a></h1>");
        builder.append("<h1>");
        builder.append("如果无法跳转，请复制链接到浏览器打开");
        builder.append("</h1>");

        //  1 ====找Spring要对象====  Spring注入了
        //  2 ====创建邮件====
        MimeMessage message = javaMailSender.createMimeMessage();
        //  3 ====创建邮件信息设置工具人====
        MimeMessageHelper messageHelper = new MimeMessageHelper(message);
        //  4 ====设置内容排版===  把正文按html解析显示  可选，这只是好看点
        //  可以理解为浏览器页面
        Multipart multipart = new MimeMultipart();
        //  可以为html标签
        BodyPart bodyPart = new MimeBodyPart();
        //  可以理解为设置Header参数 这里包括内容
        bodyPart.setContent(builder.toString(),"text/html;charset=utf-8");
        //  body装进页面
        multipart.addBodyPart(bodyPart);
        //  页面装进邮件
        message.setContent(multipart);
        //  5 ====设置邮件信息====
        //  发件人
        messageHelper.setFrom("1347881231@qq.com");
        //  收件人
        messageHelper.setTo("a1347881231@163.com");
        //  主题
        messageHelper.setSubject("主题");
        //  正文
//        messageHelper.setText("测试Spring发送邮箱");
        //  6 ====发送====
        javaMailSender.send(message);

        System.out.println("Spring发送邮件完成");
    }


}

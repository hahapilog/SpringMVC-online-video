package com.hj.onlinevideo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.ToolDao;
import com.hj.onlinevideo.entity.Tool;
import com.hj.onlinevideo.entity.ToolsType;
import com.hj.onlinevideo.service.ToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ToolServiceImpl implements ToolService {
    @Autowired
    ToolDao toolDao;

    //  查所有
    @Override
    public PageInfo<Tool> findToolAll(int page) {
        HashMap map = new HashMap();
        map.put("sort", 1);
        PageHelper.startPage(page, 8);
        List<Tool> list = toolDao.findToolAll(map);
        PageInfo<Tool> pageInfo = new PageInfo<>(list, 3);
        return pageInfo;
    }

    // 按条件查
    public PageInfo<Tool> findToolByType(int typeId,int page) {
        Map map = new HashMap<>();
        map.put("toolsTypeId", typeId);
        PageHelper.startPage(page, 8);
        List<Tool> list = toolDao.findToolByCondition(map);
        PageInfo<Tool> pageInfo = new PageInfo<>(list, 3);
        return pageInfo;
    }


}

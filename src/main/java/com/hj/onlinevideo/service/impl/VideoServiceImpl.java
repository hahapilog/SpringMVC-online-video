package com.hj.onlinevideo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.VideoDao;
import com.hj.onlinevideo.entity.Course;
import com.hj.onlinevideo.entity.Video;
import com.hj.onlinevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    VideoDao videoDao;

    // 按关键字找视频
    @Override
    public PageInfo<Video> findCourseByKeyword(String keyword, int page) {

        Map map = new HashMap();
        map.put("videoName", keyword);
        map.put("flag", 1);
        PageHelper.startPage(page, 8);
        List<Video> videoList = videoDao.findVideoByCondition(map);
        return new PageInfo<Video>(videoList, 4);
    }

    //  按课程ID找视频
    @Override
    public List<Video> findCourseVideo(int courseId) {
        Map map = new HashMap();
        map.put("courseId", courseId);
        map.put("flag", 1);
        List<Video> list = videoDao.findVideoByCondition(map);
        return list;
    }

}

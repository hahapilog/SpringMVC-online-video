package com.hj.onlinevideo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.CourseDao;
import com.hj.onlinevideo.entity.Course;
import com.hj.onlinevideo.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseDao courseDao;
    @Override
    public PageInfo<Course> findIndexNewestCourse() {
        HashMap map = new HashMap();
        map.put("sort", 1);
        List<Course> list = courseDao.findCourseByCondition(map);
        PageInfo<Course> pageInfo = new PageInfo<>(list,3);
        return pageInfo;
    }

    @Override
    public List<Course> findCourseById(List list) {
        return courseDao.findCourseByIds(list);
    }

    @Override
    public PageInfo<Course> findIndexCourseType(int type) {
        HashMap map = new HashMap();
        map.put("typeId", type);
        List<Course> list = courseDao.findCourseByCondition(map);
        PageInfo<Course> pageInfo = new PageInfo<>(list,3);
        return pageInfo;
    }

}

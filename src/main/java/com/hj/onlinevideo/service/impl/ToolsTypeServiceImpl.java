package com.hj.onlinevideo.service.impl;

import com.hj.onlinevideo.dao.ToolsTypeDao;
import com.hj.onlinevideo.entity.ToolsType;
import com.hj.onlinevideo.service.ToolsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToolsTypeServiceImpl implements ToolsTypeService {
    @Autowired
    ToolsTypeDao toolsTypeDao;

    @Override
    public List<ToolsType> findToolsTypeAll() {
        return toolsTypeDao.findToolsTypeAll();
    }
}

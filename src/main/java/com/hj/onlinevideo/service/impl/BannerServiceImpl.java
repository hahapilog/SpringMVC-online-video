package com.hj.onlinevideo.service.impl;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.BannerDao;
import com.hj.onlinevideo.entity.Banner;
import com.hj.onlinevideo.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    BannerDao bannerDao;

    @Override
    public List<Banner> findIndexBanner() {
        Map map = new HashMap<>();
        map.put("type", 1);
        List<Banner> list = bannerDao.findBannerByCondition(map);
        return list;
    }
}

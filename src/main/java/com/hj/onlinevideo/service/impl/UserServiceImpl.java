package com.hj.onlinevideo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.UserDao;
import com.hj.onlinevideo.entity.User;
import com.hj.onlinevideo.exception.MyException;
import com.hj.onlinevideo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    //  检查邮箱
    @Override
    public User checkEmail(String email) {
        User user = new User();
        user.setEmail(email);

        return userDao.findUserByCondition(user);
    }

    //  注册
    @Override
    public User register(User user) throws Exception{

        int result = userDao.insertUser(user);
        if (result == 1) {

            return userDao.findUserByCondition(user);
        }
        return null;
    }

    //  登录
    @Override
    public User login(User user) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("email", user.getEmail());
//        map.put("password", user.getPassword());
        return userDao.findUserByCondition(user);
    }

    //  更改密码
    @Override
    public int changePassword(User user) {
        return userDao.updateUser(user);
    }


    //  按页查找
    @Override
    public PageInfo<User> findUserByPage(int page) {
        PageHelper.startPage(page, 10);
        Map map = new HashMap();
        map.put("flag", 1);
        List<User> list = userDao.findUserByCondition(map);
        PageInfo<User> pageInfo = new PageInfo<>(list, 6);
        return pageInfo;
    }

    //  删除用户
    @Override
    public int deleteUser(int userId) {
        Map map = new HashMap();
        map.put("flag", 0);
        map.put("id", userId);
        int result = userDao.updateUser(map);
        return result;
    }
}

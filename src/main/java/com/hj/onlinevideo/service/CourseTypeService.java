package com.hj.onlinevideo.service;

import com.hj.onlinevideo.entity.CourseType;

import java.util.List;

public interface CourseTypeService {
    List<CourseType> courseTypeAll();
}

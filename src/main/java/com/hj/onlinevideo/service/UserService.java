package com.hj.onlinevideo.service;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.User;

public interface UserService {
    //  检查邮箱
    User checkEmail(String email);
    //  注册
    User register(User user) throws Exception;
    //  登录
    User login(User user);
    //  更改密码
    int changePassword(User user);
    //  按页查找
    PageInfo<User> findUserByPage(int page);
    //  删除用户
    int deleteUser(int userId);
}

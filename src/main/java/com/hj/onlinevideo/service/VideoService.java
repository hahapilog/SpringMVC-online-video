package com.hj.onlinevideo.service;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.Video;

import java.util.List;
import java.util.Map;

public interface VideoService {
    //  按课程ID找视频
    List<Video> findCourseVideo(int courseId);

    // 按关键字找视频
    PageInfo<Video> findCourseByKeyword(String keyword, int page);
}

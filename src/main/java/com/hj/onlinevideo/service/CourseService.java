package com.hj.onlinevideo.service;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.Course;

import java.util.List;

public interface CourseService {
    PageInfo<Course> findIndexNewestCourse();

    PageInfo<Course> findIndexCourseType(int type);

    List<Course> findCourseById(List list);
}

package com.hj.onlinevideo.service;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dao.ToolDao;
import com.hj.onlinevideo.entity.Tool;
import com.hj.onlinevideo.entity.ToolsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ToolsTypeService {
    // 查全部
    List<ToolsType> findToolsTypeAll();
}

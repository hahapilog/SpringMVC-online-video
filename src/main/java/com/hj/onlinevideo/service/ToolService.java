package com.hj.onlinevideo.service;

import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.Tool;


public interface ToolService {
    //  查全部
    PageInfo<Tool> findToolAll(int page);

    // 按条件查
    PageInfo<Tool> findToolByType(int typeId,int page);
}

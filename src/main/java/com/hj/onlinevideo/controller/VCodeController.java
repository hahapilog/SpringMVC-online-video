package com.hj.onlinevideo.controller;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.hj.onlinevideo.dto.ResponseData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

@Controller
public class VCodeController {
    String[] vCodeSequence = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};


    //  注册验证码
    @RequestMapping("/registCaptcha")
    public void registCaptcha(HttpServletResponse response, HttpSession session) {
        String captcha = responseImg(response);
        session.setAttribute("registCaptcha", captcha);
    }
    //  检查注册验证码是否正确
    @ResponseBody
    @RequestMapping("/checkRegistCaptcha")
    public ResponseData checkVcode(String captcha,HttpSession session) {
        ResponseData rd = new ResponseData();
        String sVcode = (String) session.getAttribute("registCaptcha");
        System.out.println("验证请求:" + captcha);
        System.out.println("Session取出："+sVcode);
        if (StrUtil.isEmpty(captcha) || !captcha.equals(sVcode)) {
            //  为空 或 不相等    失败
            rd.setrCode(-1);
        } else {
            //  成功
            rd.setrCode(1);
            session.removeAttribute("registCaptcha");
        }
        System.out.println("响应：" + rd);
        return rd;
    }

    //  找回密码验证码
    @ResponseBody
    @RequestMapping("/retrievePasswordCaptcha")
    public void retrievePasswordCaptcha(HttpServletResponse response, HttpSession session) {
        String captcha = responseImg(response);
        session.setAttribute("retrieveCaptcha", captcha);
    }




    // 生成并响应验证码
    private String responseImg(HttpServletResponse response) {
        //  设置响应头 jpeg
        response.setContentType("image/jpeg");
        //  设置响应不缓存
        response.setHeader("Pragma","no-cache");
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Expires", "-1");

        //生成验证码
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            //  随机一个数
            String rdStr = (String) vCodeSequence[RandomUtil.randomInt(9)];
            code.append(rdStr);
        }

        //  通过流响应
        try {
            ServletOutputStream sos = response.getOutputStream();
            ImageIO.write(generateImage(code.toString()), "jpeg", sos);
            sos.flush();
            sos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return code.toString();
    }

    //  生成图片
    private BufferedImage generateImage(String vcode) {
        //  设置图片生成属性
        BufferedImage bufferedImage = new BufferedImage(160,80,BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bufferedImage.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.drawRect(0,0,160,80);
        Font font = new Font("宋体",Font.BOLD,60);
        graphics.setFont(font);
        //  生成图片
        for (int i = 0; i < 4; i++) {
            //  随机生成一个颜色
            graphics.setColor(new Color(RandomUtil.randomInt(255),RandomUtil.randomInt(255),RandomUtil.randomInt(255)));
            //  画到image里
            graphics.drawString(String.valueOf(vcode.charAt(i)), (i + 1) * 25, 60);
        }
        System.out.println("生成的:" + vcode);
        return bufferedImage;
    }
}

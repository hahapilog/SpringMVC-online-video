package com.hj.onlinevideo.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.*;
import com.hj.onlinevideo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PageController {

    @Autowired
    CourseService courseService;

    @Autowired
    BannerService bannerService;

    @Autowired
    CourseTypeService courseTypeService;

    @Autowired
    ToolsTypeService toolsTypeService;

    @Autowired
    ToolService toolService;

    @Autowired
    VideoService videoService;


    //  首页
    @RequestMapping("/")
    public String index(Model model) {
        //  导航条焦点 1 首页 2 课程 3 会员 4 直播 5 工具
        model.addAttribute("navFocus", 1);

        System.out.println("直接返回index.jsp");
        //  Banner
        List<Banner> bannerList = bannerService.findIndexBanner();
        //  最新课程
        PageHelper.startPage(1, 4);
        PageInfo<Course> newestCourse = courseService.findIndexNewestCourse();
        //  指定类型课程
        PageHelper.startPage(1, 4);
        PageInfo<Course> type4 = courseService.findIndexCourseType(3);
        model.addAttribute("bannerList", bannerList);
        model.addAttribute("newestCourse", newestCourse);
        model.addAttribute("type4", type4);
        return "index";
    }

    //  课程
    @RequestMapping("/courseList")
    public String courseList(Model model) {
//        PageHelper.startPage(1, 8);
//        PageInfo<Course>
//        model.addAttribute("courses", courses);
//        return "courseList";
        return null;
    }

    //  更多
    @RequestMapping("/courseList/{typeId}")
    public String moreCourse(@PathVariable int typeId, @RequestParam(defaultValue = "1") int page, Model model) {
        //  导航条焦点
        model.addAttribute("navFocus", 2);
        //  分类信息
        List<CourseType> courseTypes = courseTypeService.courseTypeAll();
        //  课程列表
        PageHelper.startPage(page, 8);
        PageInfo<Course> courses = null;
        if (typeId == 0) {
            courses = courseService.findIndexNewestCourse();
        } else {
            courses = courseService.findIndexCourseType(typeId);
        }
        model.addAttribute("courses", courses);
        model.addAttribute("typeId", typeId);
        model.addAttribute("courseTypes", courseTypes);
        return "courseList";
    }

    //  会员
    @RequestMapping("/vip")
    public String vip(Model model) {
        //  导航条焦点
        model.addAttribute("navFocus", 3);

        return "vip";
    }

    //  工具
    @RequestMapping("/tools/{typeId}")
    public String tools(Model model,
                        @PathVariable int typeId,
                        @RequestParam(defaultValue = "1") int page) {
        //  导航条焦点
        model.addAttribute("navFocus", 5);
        //  所有工具分类
        List<ToolsType> toolsTypeList = toolsTypeService.findToolsTypeAll();

        PageInfo<Tool> toolList = null;
        if (typeId == 0) {
            //  所有工具    降序
            toolList = toolService.findToolAll(page);
        } else {
            //  按ID
            toolList = toolService.findToolByType(typeId, page);
        }
        model.addAttribute("toolsTypeList", toolsTypeList);
        model.addAttribute("toolList", toolList);
        model.addAttribute("typeId", typeId);
        return "tools";
    }

    //  课程视频
    @RequestMapping("/courseVideo/{courseId}")
    public String courseVideo(Model model, @PathVariable int courseId) {
        List<Video> videoList = videoService.findCourseVideo(courseId);
        List list = new ArrayList();
        list.add(courseId);
        Course course = courseService.findCourseById(list).get(0);
        model.addAttribute("course", course);
        model.addAttribute("videoList", videoList);
        return "courseVideo";
    }

    // 搜索
    @RequestMapping("/search/{keyword}/{page}")
    public String search(Model model, @PathVariable String keyword, @PathVariable @RequestParam(defaultValue = "1") int page) {
        PageInfo<Video> videoPageInfo = videoService.findCourseByKeyword(keyword, page);
        model.addAttribute("videoPageInfo", videoPageInfo);
        model.addAttribute("keyword", keyword);
        return "videoList";
    }

    // 后台
    @RequestMapping("/backstage")
    public String backstage() throws Exception {
        int a = 1/0;
        return "backstage";
    }
    // 测试静态资源html
    @RequestMapping("/html")
    public String html() throws Exception {
        return "redirect:123.html";
    }
}

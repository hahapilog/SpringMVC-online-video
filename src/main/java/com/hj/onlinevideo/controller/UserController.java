package com.hj.onlinevideo.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.dto.LoginToken;
import com.hj.onlinevideo.dto.ResponseData;
import com.hj.onlinevideo.dto.Token;
import com.hj.onlinevideo.entity.User;
import com.hj.onlinevideo.exception.MyException;
import com.hj.onlinevideo.service.UserService;
import com.hj.onlinevideo.service.impl.SendMailService;
import com.hj.onlinevideo.util.Constants;
import com.hj.onlinevideo.util.VideoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    SendMailService sendMailService;

    @RequestMapping("/test")
    public String test(HttpSession session) {
        User user = (User) session.getAttribute(Constants.SESSION_LOGIN_USER);
        System.out.println(user);
        session.invalidate();
        return "index";
    }

    //  登录
    @ResponseBody
    @RequestMapping("/login")
    public ResponseData login(User user, String autoLogin, HttpSession session,
                              HttpServletRequest request, HttpServletResponse response) {
        System.out.println("autoLogin:" + autoLogin);
        ServletContext application = session.getServletContext();
        User result = userService.login(user);
        ResponseData<User> rd = new ResponseData<>();
        if (result != null) {
            //  登录成功
            session.setAttribute(Constants.SESSION_LOGIN_USER, user);
            rd.setrCode(1);
            User data = new User();
            data.setEmail(user.getEmail());
            rd.setData(data);
            //  如果有自动登录
            if ("true".equals(autoLogin)) {
                System.out.println("设置自动登录");
                Map<String, LoginToken> loginTokenMap = (Map<String, LoginToken>) application.getAttribute(Constants.AUTO_LOGIN_TOKEN);
                if (loginTokenMap == null) {
                    loginTokenMap = new HashMap<String, LoginToken>();
                    application.setAttribute(Constants.AUTO_LOGIN_TOKEN, loginTokenMap);
                }
                LoginToken token = VideoUtil.generateToken(request, result);
                loginTokenMap.put(token.generateMD5(), token);
                //  自动登录cookie
                Cookie cookie = new Cookie(Constants.AUTO_LOGIN_TOKEN, token.generateMD5());
                cookie.setMaxAge(60 * 60 * 48);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        } else {
            rd.setrCode(-1);
        }

        return rd;
    }

    //  注册
    @RequestMapping("/regist")
    public String regist(User user, String vcode, HttpSession session) throws Exception {
        String sVcode = (String) session.getAttribute("vcode");
        if (StrUtil.isEmpty(vcode) || StrUtil.isEmpty(sVcode) || !sVcode.equals(vcode)) {
            //  空 验证不通过
            throw new MyException("验证码为空或不正确");

        }
        //  正则验证 手机 邮箱 密码
        String mobile = user.getMobile();
        String email = user.getEmail();
        String password = user.getPassword();
        boolean mobileMatch = ReUtil.isMatch("^1[3-9]\\d{9}$", mobile);
        boolean emailMatch = ReUtil.isMatch("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?", email);
        boolean passwordMatch = ReUtil.isMatch("^(?=.*[A-Za-z].*)[a-zA-Z0-9_]{8,16}$", password);
        if (!mobileMatch || !emailMatch || !passwordMatch) {
            //  不通过
            throw new MyException("手机 邮箱 密码 正则验证不正确");

        }
        //  验证通过 写入数据库
        System.out.println("验证通过");
        user.setVipFlag(0);
        user.setUsername("好好学习，天天向上");
        ResponseData rd = checkEmail(email);
        if (rd.getrCode() != 1) {
            //  邮箱已注册
            throw new MyException("邮箱已注册");

        }
        User resultUser = userService.register(user);
        if (resultUser != null) {
            //  注册成功
            session.setAttribute(Constants.SESSION_LOGIN_USER, resultUser);
        } else {
            throw new MyException("数据库写入异常");

        }
        System.out.println(user);
        return "index";
    }

    //  退出
    @RequestMapping("/exit")
    public String exit(HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        //  1 移除application中的token
        ServletContext application = session.getServletContext();
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (Constants.AUTO_LOGIN_TOKEN.equals(cookie.getName())) {
                Map tokenMap = (Map) application.getAttribute(Constants.AUTO_LOGIN_TOKEN);
                tokenMap.remove(cookie.getValue());
                System.out.println("token已清除");
            }
        }
        //  2 移除session中的user
        session.removeAttribute(Constants.SESSION_LOGIN_USER);
        System.out.println("User已清除");
        //  3 使tokenCookie失效    覆盖原来的Cookie
        Cookie cookie = new Cookie(Constants.AUTO_LOGIN_TOKEN, "invalid");
        cookie.setMaxAge(1);
        cookie.setPath("/");
        response.addCookie(cookie);
        System.out.println("Cookie已失效");
        return "redirect:index";
    }

    //  验证Email是否已注册
    @ResponseBody
    @RequestMapping("/checkEmail")
    public ResponseData checkEmail(String email) {
        User user = userService.checkEmail(email);
        ResponseData rd = new ResponseData(-1, "email already registered");
        if (user == null) {
            rd.setrCode(1);
            rd.setMessage("ok");
        }
        return rd;
    }

    //  检查验证码 发送邮件
    @ResponseBody
    @RequestMapping("/sendEmail")
    public ResponseData checkRetrieveCaptcha(String email, String captcha, HttpSession session) throws MessagingException {
        ResponseData rd = new ResponseData();
        String sessionCode = (String) session.getAttribute("retrieveCaptcha");
        if (StrUtil.isEmpty(captcha) || !captcha.equals(sessionCode)) {
            //  为空 或 不相等    验证失败
            rd.setrCode(-1);
            return rd;
        }
        User user = new User();
        user.setEmail(email);
        user = userService.login(user);
        if (user == null) {
            //  邮箱未注册
            rd.setrCode(-2);
        } else {
            //  邮箱已注册  验证成功 发送邮件
            rd.setrCode(1);
            session.removeAttribute("retrieveCaptcha");
            sendMailService.sendMail(VideoUtil.generateRetrieveLink(email));
        }
        return rd;
    }

    //    检查链接
    @RequestMapping("/changPassword")
    public String changPassword(HttpSession session, String token, String email, String time) {
        if (!VideoUtil.verificationLink(token, email, time)) {
            //  跳转错误页面
            throw new MyException("此链接已失效");
        }
        //  跳转修改密码
        session.setAttribute("changePassword", email);
        return "123.html";
    }

    //  提交修改
    @RequestMapping("/commitChange")
    public String commitChange(HttpSession session, String email, String password) {
        String changeEmail = (String) session.getAttribute("changePassword");
        if (StrUtil.isEmpty(changeEmail)) {
            //  没有更改密码的请求
            throw new MyException("session中没有更改密码请求");
        }
        if (StrUtil.isEmpty(email) || StrUtil.isEmpty(password)) {
            //  数据有误
            throw new MyException("提交数据不能为空");
        }
        if (changeEmail.equals(email)) {
            User user = new User();
            user.setEmail(email);
            user = userService.login(user);
            if (userService.changePassword(user) != 1) {
                //  修改失败
                throw new MyException("数据库异常");
            }
        }
        //  修改成功
        return "123.html";
    }

    //  展示用户
    @RequestMapping("/user/{page}")
    public String findUser(@PathVariable Integer page, Model model) {
        PageInfo<User> userList = userService.findUserByPage(page);
        model.addAttribute("userList", userList);
        model.addAttribute("currentPage", page);
        return "backstage";
    }
    //  删除用户
    @RequestMapping("/user/delete/{userId}/{page}")
    public String delete(@PathVariable int userId,@PathVariable int page,Model model) {
//        PageInfo<User> userList = userService.findUserByPage(page);
//        model.addAttribute("userList",userList);
//        model.addAttribute("currentPage", page);
        userService.deleteUser(userId);
        return "redirect:/user/"+page;
    }

}

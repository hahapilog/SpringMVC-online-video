package com.hj.onlinevideo.controller;

import com.hj.onlinevideo.service.ToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ToolController {

    @Autowired
    ToolService toolService;

    @RequestMapping("/testAll")
    public String All() {
        System.out.println(toolService.findToolAll(1));
        return "index.jsp";
    }
}

package com.hj.onlinevideo.util;

public class Constants {
    //  自动登录 application中tokens的key
    //  自动登录 Cookie中的name
    public static String AUTO_LOGIN_TOKEN = "auto_login_token";
    //  session中User的key
    public static String SESSION_LOGIN_USER = "login_user";
    //  找回密码私钥
    public static String RETRIEVE_PASSWORD_PRIVATE_KEY = "asdf654as5d6f41as56f1";
}

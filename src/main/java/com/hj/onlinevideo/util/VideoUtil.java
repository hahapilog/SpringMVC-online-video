package com.hj.onlinevideo.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.hj.onlinevideo.dto.LoginToken;
import com.hj.onlinevideo.entity.User;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

public class VideoUtil {
    /**
     * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址,
     * <p>
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
     * <p>
     * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
     * 192.168.1.100
     * <p>
     * 用户真实IP为： 192.168.1.110
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


    /**
     *   验证Token
     *
     * @param request
     * @return
     */
    public static User validationToken(HttpServletRequest request) {
        System.out.println("验证token执行");
        ServletContext application = request.getSession().getServletContext();
        Cookie[] cookies = request.getCookies();
        Map tokenMap = (Map) application.getAttribute(Constants.AUTO_LOGIN_TOKEN);
        Cookie tokenCookie = null;
        for (Cookie cookie : cookies) {
            String key = cookie.getName();
            if (Constants.AUTO_LOGIN_TOKEN.equals(key)) {
                tokenCookie = cookie;
                break;
            }
        }
        //  判断Cookie和tokenMap是否存在
        if (tokenCookie == null || tokenMap == null) {
            return null;
        }
        //  判断是否已经退出登录
        LoginToken loginToken = (LoginToken) tokenMap.get(tokenCookie.getValue());
        if (loginToken == null) {
            return null;
        }

        //  判断 MD5
        String md5 = tokenCookie.getValue();
        String tokenMd5 = loginToken.generateMD5();
        if (!StrUtil.isEmpty(md5) || !StrUtil.isEmpty(tokenMd5) || !md5.equals(tokenMd5)) {
            return null;
        }
        //  判断ip
        String ip = VideoUtil.getIpAddress(request);
        String tokenIp = loginToken.getIp();
        if (!StrUtil.isEmpty(ip) || !StrUtil.isEmpty(tokenIp) || !ip.equals(tokenIp)) {
            return null;
        }
        //  判断浏览器信息
        String userAgent = request.getHeader("User-Agent");
        String tokenUserAgent = loginToken.getUserAgent();
        if (!StrUtil.isEmpty(userAgent) || !StrUtil.isEmpty(tokenUserAgent) || !userAgent.equals(tokenUserAgent)) {
            return null;
        }
        //  判断时间
        long tokenGenerateTime = DateUtil.parseDate(loginToken.getNow()).getTime();
        long currentTime = System.currentTimeMillis();
        //  超时时间48小天    48 * 60 * 60 * 1000 = 172800000毫秒
        if(currentTime - tokenGenerateTime > 172800000) {
            return null;
        }


        //  全部成功 返回User
        System.out.println("取出的User:" + loginToken.getUser());
        return loginToken.getUser();
    }


    /**
     *  生成Token
     *
     * @param request
     * @param user
     * @return
     */
    public static LoginToken generateToken(HttpServletRequest request, User user) {
        LoginToken token = new LoginToken();
        token.setIp(getIpAddress(request));
        token.setNow(DateUtil.now());
        token.setUserAgent(request.getHeader("User-Agent"));
        token.setUser(user);
        System.out.println("存入的User:" + user);
        return token;
    }

    /**
     *  生成找出密码参数链接  token=(email + time + privateKey = MD5)&email=?&time=?
     */
    public static String generateRetrieveLink(String email) {
        long time = System.currentTimeMillis();
        String token = DigestUtil.md5Hex(email + time + Constants.RETRIEVE_PASSWORD_PRIVATE_KEY);
        StringBuilder builder = new StringBuilder();
        builder.append("http://127.0.0.1:9090/changPassword?");
        builder.append("token=");
        builder.append(token);
        builder.append("&");
        builder.append("email=");
        builder.append(email);
        builder.append("&");
        builder.append("time=");
        builder.append(time);
        return builder.toString();
    }

    /**
     * 验证找出密码链接是否合法
     */
    public static boolean verificationLink(String token, String email, String time) {
        String MD5 = DigestUtil.md5Hex(email + time + Constants.RETRIEVE_PASSWORD_PRIVATE_KEY);
        long currentTime = System.currentTimeMillis();
        long linkTime = Long.parseLong(time);
        if (StrUtil.isEmpty(token) || StrUtil.isEmpty(email) || StrUtil.isEmpty(time)) {
            //  参数异常
            return false;
        }
        if (MD5.equals(token) && currentTime - linkTime < 1800000) {
            return true;
        }
        return false;
    }
}

import cn.hutool.core.date.DateUtil;
import com.hj.onlinevideo.controller.UserController;
import com.hj.onlinevideo.dao.*;
import com.hj.onlinevideo.entity.*;
import com.hj.onlinevideo.service.impl.SendMailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-*.xml")
public class TestDao {
    @Autowired
    ToolDao toolDao;

    @Test
    public void getAll() {
        List<Tool> list = toolDao.findToolAll(null);
        System.out.println(list);
    }

    @Test
    public void insert() {
        Tool tool = new Tool();
        for (int i = 1; i <= 50; i++) {
            tool.setName("正则" + i);
            tool.setToolsTypeId(1);
            toolDao.insertTool(tool);
        }

    }

    @Autowired
    BannerDao bannerDao;
    @Test
    public void bannerInsert() {
        Banner banner = new Banner();
        banner.setImgUrl("abc");
        bannerDao.insertBanner(banner);
    }

    @Autowired
    CourseDao courseDao;
    @Test
    public void insertCourse() {
        for(int i = 0; i < 50; i++) {
            Course course = new Course();
            course.setCourseName("springboot基础"+ (i+1));
            course.setShowIco("../static/imgs/356.jpg");
            course.setFlag(1);
            course.setTypeId(3);
            course.setCoursewareUrl("课件" + (i + 1));
            courseDao.insertCourse(course);
        }
    }

    @Test
    public void selectOne() {
        Map map = new HashMap<>();
        map.put("courseName","aaa");
        System.out.println(courseDao.findCourseByCondition(map));
    }

    @Autowired
    CourseTypeDao courseTypeDao;
    @Test
    public void t3() {
        CourseType courseType = new CourseType();
        courseType.setTypeName("呱呱唧唧");
        courseTypeDao.insertCourseType(courseType);
    }

    @Autowired
    ToolsTypeDao toolsTypeDao;
    @Test
    public void t4() {
        ToolsType toolsType = new ToolsType();
        toolsType.setName("呱唧");
        toolsTypeDao.insertToolsType(toolsType);
    }

    @Autowired
    UserDao userDao;
    @Test
    public void t5() {
        for(int i = 0; i < 20; i++) {
            User user = new User();
            user.setUsername("唧唧喳喳" + i);
            user.setEmail("1234@qq.com");
            user.setFlag(1);
            user.setVipFlag(1);
            userDao.insertUser(user);
        }
//        List<User> list = userDao.findUserAll();
//        for(User u : list) {
//
//            System.out.println(u.getFlag());
//        }

    }

    @Autowired
    VideoDao videoDao;
    @Test
    public void t6() {
//        Video video = new Video();
//        video.setVideoName("有吃有喝");
//        videoDao.insertVideo(video);
        List<Video> list = videoDao.findVideoByCondition(new HashMap());
        for (Video v : list) {
            System.out.println(v);

        }
    }

    @Autowired
    UserController userController;
    @Test
    public void t7() {
        long systemLongTime = System.currentTimeMillis();
        Date systemDate = DateUtil.date(systemLongTime);
        String utilDate = DateUtil.now();
        long utilLongTime = DateUtil.parse(utilDate).getTime();
        System.out.println(systemLongTime);
        System.out.println(utilLongTime);
        System.out.println(systemDate);
        System.out.println(utilDate);
    }
    //  测试发送邮件
    @Autowired
    SendMailService sendMailService;
    @Test
    public void t8() {
        try {
            sendMailService.sendMail("https://www.baidu.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

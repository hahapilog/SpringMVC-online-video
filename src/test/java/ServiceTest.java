import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hj.onlinevideo.entity.Course;
import com.hj.onlinevideo.service.impl.CourseServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-*.xml")
public class ServiceTest {
    @Autowired
    CourseServiceImpl courseService;
    @Test
    public void t1() {
        PageHelper.startPage(1, 4);
        PageInfo<Course> list = courseService.findIndexNewestCourse();
        for (Course c : list.getList()) {
            System.out.println(c);
        }

    }
}
